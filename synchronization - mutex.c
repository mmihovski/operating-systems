﻿#include <stdio.h>
#include <Windows.h>

struct threadinfo
{
	int data;
	HANDLE h;
};

struct threadinfo array[1000];
int k;

DWORD WINAPI function(void *p);

int main()
{
	int  i;
	HANDLE th[100];

	printf("How many threads ? ");
	scanf_s("%d", &k);

	for (i = 0; i < 20; i++)
	{
		array[i].h = CreateMutex(NULL, 0, NULL);
	}

	array[0].data = 0; array[4].data = 3; array[8].data = 2; array[12].data = 4; array[16].data = 6;
	array[1].data = 0; array[5].data = 3; array[9].data = 4; array[13].data = 4; array[17].data = 7;
	array[2].data = 1; array[6].data = 3; array[10].data = 4; array[14].data = 4; array[18].data = 7;
	array[3].data = 1; array[7].data = 2; array[11].data = 4; array[15].data = 5; array[19].data = 7;

	for (i = 0; i < k; i++)
	{
		th[i] = CreateThread(NULL, 0, function, (void *)i, 0, 0);
	}

	for (i = 0; i < k; i++)
	{
		WaitForSingleObject(th[i], INFINITE);
	}

	printf("END");
	fflush(stdin);
	getchar();
	return 0;
}

DWORD WINAPI function(void *p)
{
	int j, count;
	int n = (int)p;

	for (j = 0, count = 0; j < 20; j++)
	{
		WaitForSingleObject(array[j].h, INFINITE);
		if (array[j].data == n)
		{
			count++;
			printf("coincidence %d \n", n);
		}
		ReleaseMutex(array[j].h);
		if (count == 5)
		{
			break;
		}
	}
	return 0;
}

