﻿#include <stdio.h>
#include <windows.h>

#define N 2000000

struct aos
{
	int x;
	int y;
	int z;
};

struct soa
{
	int x[N];
	int y[N];
	int z[N];
};
 
double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
	LARGE_INTEGER li;
	if (!QueryPerformanceFrequency(&li))
	{
		printf("Failed");
	}


	PCFreq = (double) (li.QuadPart) / 1000.0;

	QueryPerformanceCounter(&li);
	CounterStart = li.QuadPart;
}

double GetCounter()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return (double) (li.QuadPart - CounterStart) / PCFreq;
}

int main()
{
	struct aos A[N];
	struct soa B;
	int i, n;
	double time_aos;
	double time_soa;

	for (i = 0; i < N; i++)
	{
		A[i].x = i + 1;
		A[i].y = i + 1;
		A[i].z = i + 1;

		B.x[i] = i + 1;
		B.y[i] = i + 1;
		B.z[i] = i + 1;
	}

	StartCounter();
	for (i = 0; i < N; i++)
	{
		n = A[i].x;
		n = A[i].y;
		n = A[i].z;
	}
	time_aos = GetCounter();

	printf("AoS = %f sec\n", time_aos);

	StartCounter();
	for (i = 0; i < N; i++)
	{
		n = B.x[i];
		n = B.y[i];
		n = B.z[i];
	}
	time_soa = GetCounter();

	printf("SoA = %f sec\n", time_soa);

	fflush(stdin);
	getchar();
	return 0;
}
