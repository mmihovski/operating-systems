﻿#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

struct threadinfo
{
	HANDLE handle;
	DWORD id;
};

DWORD WINAPI ThreadFunction(void *p);

int main()
{
	int br, i;
	struct threadinfo *threads;

	printf("How many threads to create: ");
	scanf_s("%d", &br);

	if ((threads = (struct threadinfo *)malloc(sizeof(struct threadinfo)*br)) == NULL)
	{
		return 1;
	}

	for (i = 0; i < br; i++)
	{
		threads[i].handle = CreateThread(NULL, 0, ThreadFunction, (void*)&threads[i], 0 /*CREATE_SUSPENDED*/, &threads[i].id);
	}

	fflush(stdin);
	getchar();
	return 0;
}

DWORD WINAPI ThreadFunction(void *p)
{
	int i;
	struct threadinfo *arg = (struct threadinfo *)p;

	printf("Thread %d start \n", arg->id);

	for (i = 0; i < 5000; i++);
	

	printf("Thread %d end \n", arg->id);

	return 0;
}

