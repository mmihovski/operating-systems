﻿#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

struct threadinfo
{
	HANDLE handle;
	DWORD id;
};

DWORD WINAPI function(void *p);

int counter = 0, checks, it;
DWORD TID;
HANDLE hs;

int main()
{
	int br, i;
	struct threadinfo *threads;
	HANDLE *Threads_h;

	hs = CreateSemaphore(NULL, 1, 1, NULL);
	//hs = CreateMutex(NULL, 0, NULL);

	printf("How many threads ? ");
	scanf_s("%d", &br);
	printf("How many iterations ? ");
	scanf_s("%d", &it);
	printf("How many  checks ? ");
	scanf_s("%d", &checks);

	if ((threads = (struct threadinfo *)malloc(sizeof(struct threadinfo)*br)) == NULL)
	{
		return 1;
	}

	if ((Threads_h = (HANDLE *)malloc(sizeof(HANDLE)*br)) == NULL)
	{
		return 1;
	}

	for (i = 0; i < br; i++)
	{
		threads[i].handle = CreateThread(NULL, 0, function, &threads[i], CREATE_SUSPENDED, &threads[i].id);
		Threads_h[i] = threads[i].handle;
	}

	for (i = 0; i < br; i++)
	{
		ResumeThread(threads[i].handle);
	}

	/*
	
	WaitForMultipleObjects(br, Threads_h, TRUE, INFINITE);

	for (i = 0; i < br; i++)
	{	
		CloseHandle(Threads_h[i]);
	}
	*/
	printf("Unsynchronized access: %d\n", counter);

	fflush(stdin);
	getchar();
	return 0;
}

DWORD WINAPI function(void *p)
{
	struct threadinfo *arg = (struct threadinfo *) p;
	int i, j;

	for (i = 0; i < checks; i++)
	{
		WaitForSingleObject(hs, INFINITE);
		TID = arg->id;
		for (j = 0; j < it; j++);

		if (TID != arg->id)
		{
			counter++;
		}
		ReleaseSemaphore(hs, 1, NULL);
		//ReleaseMutex(hs);
	}

	printf("Thread %ul end.\n", arg->id);
	return 0;
}

