﻿#include<stdio.h>
#include<Windows.h>

struct threadinfo
{
	HANDLE handle;
	DWORD id;
	int terminate;
};

struct item
{
	int data, empty;
};

DWORD WINAPI prod_thread(LPVOID arg);
DWORD WINAPI cons_thread(LPVOID arg);

struct item *buf;
int br_arr;
int counter_prod = 0, counter_cons = 0;

HANDLE sem_full, sem_empty, mutex;

int main()
{
	struct threadinfo *th_prod, *th_cons;
	int br_prod, br_cons, i;

	printf("How many items ? ");
	scanf_s("%d", &br_arr);
	printf("How many produser threads ? ");
	scanf_s("%d", &br_prod);
	printf("How many consumer threads ? ");
	scanf_s("%d", &br_cons);

	if ((buf = (struct item *)malloc(sizeof(struct item)*br_arr)) == NULL)
	{
		return 1;
	}
	if ((th_prod = (struct threadinfo *)malloc(sizeof(struct threadinfo)*br_prod)) == NULL)
	{
		return 1;
	}
	if ((th_cons = (struct threadinfo *)malloc(sizeof(struct threadinfo)*br_cons)) == NULL)
	{
		return 1;
	}

	sem_full = CreateSemaphore(NULL, 0, br_arr, NULL);
	sem_empty = CreateSemaphore(NULL, br_arr, br_arr, NULL);
	mutex = CreateMutex(NULL, FALSE, NULL);

	for (i = 0; i < br_arr; i++)
	{
		buf[i].data = 0;
		buf[i].empty = 1;
	}

	for (i = 0; i < br_cons; i++)
	{
		th_cons[i].handle = CreateThread(NULL, 0, cons_thread, (LPVOID)&th_cons[i], 0, &th_cons[i].id);
		th_cons[i].terminate = 0;

	}

	for (i = 0; i < br_prod; i++)
	{
		th_prod[i].handle = CreateThread(NULL, 0, prod_thread, (LPVOID)&th_prod[i], 0, &th_prod[i].id);
		th_prod[i].terminate = 0;
		counter_prod++;
	}

	Sleep(5000);

	for (i = 0; i < br_prod; i++)
	{
		th_prod[i].terminate = 1;
	}
	for (i = 0; i < br_cons; i++)
	{
		th_cons[i].terminate = 1;
	}

	for (i = 0; i < br_prod; i++)
	{
		WaitForSingleObject(th_prod[i].handle, INFINITE);
	}
	for (i = 0; i < br_cons; i++)
	{
		WaitForSingleObject(th_cons[i].handle, INFINITE);
	}

	free(buf);
	free(th_prod);
	free(th_cons);

	printf("producer threads - %d\n", counter_prod);
	printf("consumer threads - %d\n", counter_cons);
	printf("END\n");

	fflush(stdin);
	getchar();
	return 0;
}

DWORD WINAPI prod_thread(LPVOID arg)
{
	struct threadinfo *argum = (struct threadinfo *)arg;
	DWORD sem_empt;
	int i;

	while (!argum->terminate)
	{
		sem_empt = WaitForSingleObject(sem_empty, 1000);
		if (sem_empt == WAIT_OBJECT_0)
		{
			for (i = 0; i < br_arr; i++)
			{
				WaitForSingleObject(mutex, INFINITE);
				if (buf[i].empty)
				{
					buf[i].data = 1;
					counter_prod++;
					//printf(" %d from thread - %ul to buf \n", buf[i].data, argum->handle);
					buf[i].empty = 0;
					ReleaseMutex(mutex);
					break;
				}
				ReleaseMutex(mutex);
			}
			ReleaseSemaphore(sem_full, 1, NULL);
		}
	}

	ReleaseSemaphore(sem_full, 1, NULL);

	return (DWORD)arg;
}

DWORD WINAPI cons_thread(LPVOID arg)
{
	struct threadinfo *argum = (struct threadinfo *)arg;
	DWORD sem_ful;
	int i;

	while (!argum->terminate)
	{
		sem_ful = WaitForSingleObject(sem_full, 1000);
		if (sem_ful == WAIT_OBJECT_0)
		{
			for (i = 0; i < br_arr; i++)
			{
				WaitForSingleObject(mutex, INFINITE);
				if (!buf[i].empty)
				{
					counter_cons++;
					//printf("%d from buf to %ul thread \n", buf[i].data, argum->id);
					buf[i].data = 0;
					buf[i].empty = 1;
					ReleaseMutex(mutex);
					break;
				}
				ReleaseMutex(mutex);
			}
			ReleaseSemaphore(sem_empty, 1, NULL);
		}
	}
	return (DWORD)arg;
}


